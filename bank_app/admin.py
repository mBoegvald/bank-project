from django.contrib import admin
from .models import CustomerRank, Customer, Account, Transaction


@admin.register(CustomerRank, Customer, Account, Transaction)
class DefaultAdmin(admin.ModelAdmin):
    pass
