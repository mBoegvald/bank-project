from rest_framework import generics
from .models import Account
from .serializers import TransferMoneyFormSerializer


class CustomerAccounts(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = TransferMoneyFormSerializer

    def get_queryset(self):
        queryset = Account.objects.filter(user=self.kwargs['pk'])
        return queryset


class AccountList(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = TransferMoneyFormSerializer
