from django.conf import settings
from django.core.exceptions import PermissionDenied
import user_agents


class BrowserFilterMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        not_allowed_browers = settings.BROWSERFILTER_MIDDLEWARE['NOT_ALLOWED_BROWERS']

        browser = user_agents.parse(
            request.META['HTTP_USER_AGENT']).browser.family

        if browser in not_allowed_browers:
            raise PermissionDenied

        return response
