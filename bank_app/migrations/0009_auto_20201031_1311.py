# Generated by Django 3.1.2 on 2020-10-31 13:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bank_app', '0008_auto_20201030_1244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transactions',
            name='account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bank_app.account'),
        ),
    ]
