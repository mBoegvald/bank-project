from django.db import models, transaction
from django.contrib.auth.models import User, Group
from decimal import Decimal
from uuid import uuid4
from bank_project import settings
from .errors import InsufficientFunds


class CustomerRank(models.Model):
    name = models.CharField(max_length=35, unique=True)
    value = models.IntegerField(unique=True)

    @classmethod
    def default_rank(cls):
        min_rank = cls.objects.all().aggregate(
            models.Min('value'))['value__min']
        return cls.objects.get(value=min_rank)

    def __str__(self):
        return f'{self.name} - {self.value}'


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    rank = models.ForeignKey(CustomerRank, on_delete=models.PROTECT)
    phone = models.CharField(max_length=35)

    @classmethod
    def create_customer(cls, username, password, email, phone, rank=CustomerRank.default_rank()):
        user = User.objects.create_user(
            username=username, email=email, password=password)
        customer = cls()
        customer.user = user
        customer.rank = rank
        customer.phone = phone
        customer.save()

        # Automatically add user to Customer group
        group = Group.objects.get(name='Customer')
        user.groups.add(group)

        # Create starter account for new customer.
        Account(user=user, name='Starter account').save()
        return user

    def update_user(self, username, email, phone):
        self.user.username = username
        self.user.email = email
        self.user.save()

        self.phone = phone
        self.save()

    @property
    def accounts(self):
        return Account.objects.filter(user=self.user)

    @property
    def can_make_loans(self):
        return self.rank.value >= settings.MINIMUM_RANK_LOAN

    def make_loan(self, amount, loan_name, selected_account):
        if self.can_make_loans:
            loan = Account(user=self.user, name=loan_name)
            loan.save()
            to_account = Account.objects.get(
                user=self.user, name=selected_account)
            Transaction.transfer(loan, to_account, Decimal(amount), is_loan=True)

    def __str__(self):
        return f'{self.user} - {self.rank}'


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=35)

    @classmethod
    def create_account(cls, user, name):
        account = cls()
        account.user = user
        account.name = name
        account.save()

    @property
    def balance(self):
        return Transaction.objects.filter(account=self).aggregate(models.Sum('amount'))['amount__sum'] or Decimal('0')

    @property
    def movements(self):

        return Transaction.objects.filter(account=self)

    def __str__(self):
        return f'{self.user} - {self.name}'


class Transaction(models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    transaction_id = models.CharField(max_length=35)
    amount = models.DecimalField(max_digits=18, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.CharField(max_length=35)

    @classmethod
    def transfer(cls, from_account, to_account, amount, is_loan=False):
        with transaction.atomic():
            if from_account.balance >= amount or is_loan:
                transaction_id = str(uuid4())
                cls(account=from_account, transaction_id=transaction_id,
                    amount=-amount, text=f"""Transaction: Credit
                    From: {from_account}
                    To: {to_account}
                    """).save()
                cls(account=to_account, transaction_id=transaction_id,
                    amount=amount, text=f"""Transaction: Debit
                    From: {to_account}
                    To: {from_account}
                    """).save()
            else:
                raise InsufficientFunds

    def __str__(self):
        return f'{self.amount} - {self.transaction_id}'
