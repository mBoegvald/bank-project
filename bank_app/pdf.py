from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
import io


def create_pdf(dict):
    customer = dict['customer'].user.username
    accounts = dict['customer'].accounts
    for account in accounts:
        buffer = io.BytesIO()
        p = canvas.Canvas(buffer)
        p.setTitle(f'{customer}-account-{account.id}.pdf')

        x = 50
        y = 10.5
        textobject1 = p.beginText()
        textobject1.setTextOrigin(x, y*inch)
        textobject1.setFont("Helvetica", 12)
        textobject1.textLines(f'Report for {account}:')
        p.drawText(textobject1)

        for i, movement in enumerate(account.movements):
            textobject2 = p.beginText()
            textobject2.setTextOrigin(x, ((y-i-0.5)*inch)-27)
            textobject2.setFont("Helvetica", 12)
            textobject2.textLines(f'{movement.text}')
            p.drawText(textobject2)

        p.showPage()
        p.save()

        buffer.seek(0)

        with open(f'files/{customer}-account-{account.id}.pdf', 'wb') as f:
            f.write(buffer.getbuffer())
            f.close()
