from rest_framework import serializers
from .models import Account


class TransferMoneyFormSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Account
