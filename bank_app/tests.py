from django.test import TestCase
from django.contrib.auth.models import Group
from .models import Customer, CustomerRank, Account, Transaction
from bank_project import settings
from django.shortcuts import get_object_or_404


class MoneySystemTestCase(TestCase):
    def setUp(self):
        ranks = settings.CUSTOMER_RANKS
        for k, v in ranks.items():
            CustomerRank.objects.get_or_create(name=k, value=v)
        Group.objects.get_or_create(name='Customer')
        Customer.create_customer(
            username="testdummy1", email="testdummy1@dummy.com", password="Password123", rank=CustomerRank.default_rank(), phone="143423423")
        Customer.create_customer(
            username="testdummy2", email="testdummy2@dummy.com", password="Password123", rank=get_object_or_404(
                CustomerRank, value=5), phone="14344324223")
        self.customer1 = Customer.objects.get(user_id=1)
        self.customer2 = Customer.objects.get(user_id=2)
        self.account1 = get_object_or_404(Account, user=self.customer1.user)
        self.account2 = get_object_or_404(Account, user=self.customer2.user)

        # Creating transaction to account2, so it has some money to transfer.
        Transaction(account=self.account2, amount=999, text="Creating fake transaction").save()

    def test_check_if_rank_can_make_loan(self):
        """Testing if a gold customer can make loans"""
        # print(f"""Customer rank = default
        # can make loan: {customer.can_make_loans}""")
        self.assertFalse(self.customer1.can_make_loans)
        self.assertTrue(self.customer2.can_make_loans)

    def test_loan_money(self):
        """Testing if loan is created """

        # Check if customer with rank = Basic can make a loan.
        self.customer1.make_loan(amount=9999, loan_name="Money for computer", selected_account="Starter account")
        self.assertFalse(self.customer1.accounts.filter(name="Money for computer").exists())

        # Check if customer with rank = Gold can make a loan.
        self.customer2.make_loan(amount=9999, loan_name="Money for computer", selected_account="Starter account")
        self.assertTrue(self.customer2.accounts.filter(name="Money for computer").exists())

    def test_transfer_money(self):
        # """Testing if transfer money works with sufficient funds"""
        Transaction.transfer(self.account2, self.account1, 500)
        self.assertTrue(self.account1.movements)
