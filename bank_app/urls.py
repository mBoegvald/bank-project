from django.urls import path
from .api import AccountList, CustomerAccounts
from . import views

app_name = 'bank_app'

urlpatterns = [
    path('', views.index, name='index'),
    path('profile/', views.profile, name='profile'),
    path('accounts/', views.accounts, name='accounts'),
    path('transfer_money/', views.transfer_money, name='transfer_money'),
    path('loans/', views.loans, name='loans'),
    path('download_pdf/<int:account_id>', views.download_pdf, name='download_pdf'),
    path('api/v1/', AccountList.as_view()),
    path('api/v1/<int:pk>', CustomerAccounts.as_view()),
    # Paths for bankers
    path('all_customers/', views.all_customers, name='all_customers'),
    path('new_customer/', views.new_customer, name='new_customer'),
    path('customer_details/<int:customer_id>', views.customer_details, name='customer_details'),
    path('create_customer_account/<int:customer_id>', views.create_customer_account, name='create_customer_account'),
]
