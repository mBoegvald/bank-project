from django.contrib.auth.decorators import user_passes_test


def group_required(group_name):
    def in_group(user):
        if bool(user.groups.filter(name=group_name)):
            return True
        return False
    return user_passes_test(in_group)
