from django.shortcuts import render, get_object_or_404, reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from decimal import Decimal
from .models import CustomerRank, Customer, Account, Transaction
from .utils import group_required
from .pdf import create_pdf
import django_rq
from django.core.files import File


@login_required
def index(request):
    return render(request, 'bank_app/index.html')


@login_required
def profile(request):
    if request.method == 'POST':
        customer_id = request.user.customer.id
        username = request.POST['username']
        email = request.POST['email']
        phone = request.POST['phone']
        customer = get_object_or_404(Customer, id=customer_id)
        customer.update_user(username, email, phone)
        return HttpResponseRedirect(reverse('bank_app:profile'))
    context = {
        'user': request.user
    }
    return render(request, 'bank_app/profile.html', context)


@group_required('Banker')
def new_customer(request):
    if request.method == 'POST':
        # Get form information
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        phone = request.POST['phone']
        if password == confirm_password:
            # create Customer - User is made inside of the classmethod "create_customer"
            Customer.create_customer(username, password, email, phone)
            return HttpResponseRedirect(reverse('bank_app:all_customers'))
    else:
        return render(request, 'bank_app/new_customer.html')


@group_required('Banker')
def all_customers(request):
    customers = Customer.objects.all()
    context = {
        'customers': customers
    }
    return render(request, 'bank_app/all_customers.html', context)


@group_required('Banker')
def customer_details(request, customer_id):
    customer = get_object_or_404(Customer, id=customer_id)
    customer_ranks = CustomerRank.objects.all()

    context = {
        'customer': customer,
        'customer_ranks': customer_ranks,
    }
    if request.method == "POST":
        rank = request.POST['rank']
        customer.rank = get_object_or_404(CustomerRank, value=rank)
        username = request.POST['username']
        email = request.POST['email']
        phone = request.POST['phone']
        customer.update_user(username, email, phone)

    return render(request, 'bank_app/customer_details.html', context)


@login_required
def accounts(request):
    customer = get_object_or_404(Customer, id=request.user.customer.id)
    django_rq.enqueue(create_pdf, {
        'customer': customer
    })
    context = {
        'customer': customer
    }
    return render(request, 'bank_app/accounts.html', context)


@group_required('Banker')
def create_customer_account(request, customer_id):
    context = {
        'customer_id': customer_id
    }
    if request.method == "POST":
        customer = get_object_or_404(Customer, id=customer_id)
        user = customer.user
        name = request.POST['name']
        Account.create_account(user, name)
        return HttpResponseRedirect(reverse('bank_app:customer_details', args=[customer_id]))
    else:
        return render(request, 'bank_app/create_customer_account.html', context)


@login_required
def transfer_money(request):
    if request.method == 'POST':
        from_account = get_object_or_404(Account, pk=request.POST['from_account'])
        to_account = get_object_or_404(Account, pk=request.POST['to_account'])
        amount = Decimal(request.POST['amount'])
        Transaction.transfer(from_account, to_account, amount)
        return HttpResponseRedirect(reverse('bank_app:accounts'))
    else:
        user_accounts = request.user.customer.accounts
        all_customers = Customer.objects.all()
        context = {
            'user_accounts': user_accounts,
            'all_customers': all_customers,
        }
        return render(request, 'bank_app/transfer_money.html', context)


@login_required
def loans(request):
    customer = get_object_or_404(Customer, id=request.user.customer.id)
    if request.method == 'POST':
        loan_name = request.POST['loan_name']
        selected_account = request.POST['account_name']
        amount = request.POST['amount']
        customer.make_loan(amount, loan_name, selected_account)
        return HttpResponseRedirect(reverse('bank_app:accounts'))
    else:
        context = {
            'customer': customer
        }
        return render(request, 'bank_app/loans.html', context)


def download_pdf(request, account_id):
    user = request.user
    path_to_file = f'files/{user}-account-{account_id}.pdf'
    f = open(path_to_file, 'rb')
    myfile = File(f)
    response = HttpResponse(myfile, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename='f'{user}-account-{account_id}.pdf'
    return response
