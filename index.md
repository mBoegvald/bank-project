---
name: Documentation for Bank project
---


# Development Environments

### Branch Setup

###### How we use branches to keep a nice overview

Upon making a new feature checkout to develop branch and create a new branch from develop.
Name the branch "feature/< branchname >".  

Make more small commits frequently to your branch instead of a few large commits.
(This rule is not always true.)

### Coding Standards

###### PEP 8 -- Style guide for python code

As for coding standards we use PEP 8 which is the coding standards of python.
PEP 8 style guide can be found on python.org

### CI/CD

###### All you need to know about our pipeline

Upon pushing to any branch our pipeline will automatically run the tests and linting.
You can see the result on the given branch in GitLab. Only successfully pipelines will be merged.

Deploying only affects the master branch so dont worry about that.

### Test Files

###### Running Test

We're currently using 3 unittests to test the follow functionalities
1. Can customer create a loan based on their rank.
2. Will the loan be created if the customer has either the rank Basic or Gold.
3. Can a customer tranfser money if he has a sufficent balance on his/hers account.

To run the tests inside Visual Studio code write `python manage.py test`.

All 3 tests will also be run during the analyse in the pipeline

### Environment

###### To join the project follow the guide underneath
In this project you need python 3.7 or newer
1. To check your version write `python3 -V`

2. To create an environment named bank_project write `python3 -m venv bank_project`

3. Navigate to the directory where your environment is located and write `source bank_project/bin/activate` to activate your environment

4. Install the required dependencies `(bank_project) $ pip install -r requirements.txt`

5. Now the project can run `python manage.py runserver`
 
