from django.urls import path
from . import views

app_name = 'invest_app'

urlpatterns = [
    path('investments/', views.investments, name="investments"),
]
