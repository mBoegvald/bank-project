from django.shortcuts import render
import requests
from bank_project import settings


def investments(request):
    url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/v2/get-quotes"

    querystring = {"symbols": settings.COMPANY_SYMBOLS, "region": "US"}

    headers = {
        'x-rapidapi-key': "731bc1f1a7msh48a19a513f6334cp1820f9jsn6fd6777c92e8",
        'x-rapidapi-host': "apidojo-yahoo-finance-v1.p.rapidapi.com"
        }

    response = requests.request("GET", url, headers=headers, params=querystring)
    response_results = response.json()["quoteResponse"]["result"]
    context = {
        'companyList': response_results,
    }
    return render(request, 'invest_app/investments.html', context)
