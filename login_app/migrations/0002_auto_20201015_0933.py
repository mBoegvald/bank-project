# Generated by Django 3.1.1 on 2020-10-15 09:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('login_app', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='passwordresetrequest',
            old_name='token',
            new_name='secret',
        ),
    ]
